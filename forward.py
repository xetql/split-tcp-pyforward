# -*- coding: utf-8 -*-
"""Python traffic forwarding

This software should run in background with a traffic redirection rule which
redirect incoming traffic from ovpn-tcp4 to :8888.
It forwards the traffic between source and the original traffic destination
and do the forwarding of the response as well.

For each accepted connection, a thread that forwards to the original destination
and another thread that forwards the response traffic to the source are created

"""

import asyncio
import socket
import struct
import argparse

# Const for getsockopt
SO_ORIGINAL_DST = 80
SO_v4_ORIGINAL_DST_N_BYTES = 28
SO_v6_ORIGINAL_DST_N_BYTES = 64
SOL_IP6 = 41
SOL_IP = socket.SOL_IP
loopback_port = ''
been_cancelled = asyncio.Event()

def get_orig_dest_ip_port(sock):
    """Get the original destination ip(v4 or v6):port from sockopt of a given socket, return a tuple (ip, port)
    Args:
        sock: the socket on which the communication takes place
    """
    import socket
    family = sock.family
    if family == socket.AF_INET:
        dst = sock.getsockopt(SOL_IP, SO_ORIGINAL_DST, SO_v4_ORIGINAL_DST_N_BYTES)
        raw_port, raw_ip = struct.unpack_from("!2xH4s", dst)
    elif family == socket.AF_INET6:
        dst = sock.getsockopt(SOL_IP6, SO_ORIGINAL_DST, SO_v6_ORIGINAL_DST_N_BYTES)
        raw_port, raw_ip = struct.unpack_from("!2xH4x16s", dst)
    else:
        raise ValueError("Unsupported sa_family_t %d" % family)
    readable_ip = socket.inet_ntop(sock.family, raw_ip)
    readable_port = int(raw_port)
    return readable_ip, readable_port


async def forward(src_reader, dst_writer, buffsize=4096):
    """Forward traffic from a source (read) socket to a destination (write) socket
    Args:
        src_reader: the socket on which we read on
        dst_writer: the socket on which we copy the content that has been read from src_reader
        buffsize  : the number of bytes read from the src_reader socket (default: 4096)
    """

    error = False
    while 1:
        try:
            request = await src_reader.read(buffsize)
            dst_writer.write(request)
            await dst_writer.drain()
            if not request:
                break
        except ConnectionResetError:
            error = True
            break

    s = dst_writer.get_extra_info('socket')
    if error:
        dst_writer.close()
    else:
        s.shutdown(socket.SHUT_WR)


async def on_connection_arrived(src_reader, src_writer):
    """Callback called for each accepted connection.
    It retrieves the original destination ip:port and open a connection on it
    then it creates a src->dst forwarding thread and a dst->src thread"""
    global loopback_port

    sock = src_writer.get_extra_info('socket')
    tasks = []
    if loopback_port:
        dip  = src_writer.get_extra_info('peername')[0]
        dport = loopback_port
    else:
        dip, dport = get_orig_dest_ip_port(sock)

    try:
        dst_reader, dst_writer = await asyncio.open_connection(dip, dport)
        tasks.append(asyncio.create_task(forward(src_reader, dst_writer)))
        tasks.append(asyncio.create_task(forward(dst_reader, src_writer)))
        await asyncio.gather(*tasks)
    except (ConnectionRefusedError, OSError) as e:
        pass


async def main():
    """Entry point, it creates the serversocket that listen on port 8888 and accept incoming connections."""
    server = await asyncio.start_server(on_connection_arrived, '', 8888)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Python traffic forwarding tool.')
    parser.add_argument('--loopback_port', dest='loopback_port', type=int, nargs='?',
                        help='Add a loopback call for clients that connect directly to port', default=0)

    args = parser.parse_args()
    loopback_port = args.loopback_port

    asyncio.run(main())
