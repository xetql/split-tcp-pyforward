import asyncio
import socket
import unittest
import struct


async def run_callback_server(callback):
    """it creates the serversocket that listen on port 8888 and accept incoming connections."""
    server = await asyncio.start_server(callback, '', 8889)
    async with server:
        await server.serve_forever()


class Test(unittest.TestCase):
    def test_send_huge_message(self):
        """Try to send a huge message"""
        MSG = b'10'*2**12

        async def test_shutdown_cb(r, w):
            m = b''

            m = await r.read(-1)
            self.assertEqual(m, MSG)

            w.write(m)
            await w.drain()
            w.get_extra_info('socket').shutdown(socket.SHUT_WR)
            w.close()
            await asyncio.sleep(0.1)

        async def main(**kwargs):
            t = asyncio.create_task(run_callback_server(test_shutdown_cb))

            await asyncio.sleep(0.1)

            r, w = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])

            w.write(MSG)
            await w.drain()
            w.get_extra_info('socket').shutdown(socket.SHUT_WR)
            m = await r.read(-1)
            t.cancel()
            w.close()
            await asyncio.sleep(0.1)

            return m

        r = asyncio.run(main(ip='127.0.0.1', port=8888))
        self.assertEqual(r, MSG)

    def test_conn_establishment(self):
        """Test if it correctly forwards a connexion tentative"""
        async def test_shutdown_cb(reader, writer):
            s = writer.get_extra_info('socket')
            s.shutdown(socket.SHUT_RDWR)
            await asyncio.sleep(0.1)

        async def main(**kwargs):
            t = asyncio.create_task(run_callback_server(test_shutdown_cb))
            await asyncio.sleep(0.1)
            try:
                r, w = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])
                data = await r.read(4096)
                t.cancel()
                return data
            except ConnectionRefusedError:
                t.cancel()
                return b'error'
        r = asyncio.run(main(ip='127.0.0.1', port=8888))
        self.assertTrue(r == b'')

    def test_MANY_conn_establishment(self):
        """Test if it handles more than 2048 connections"""
        async def test_shutdown_cb(reader, writer):
            s = writer.get_extra_info('socket')
            data = await reader.read(-1)
            s.shutdown(socket.SHUT_WR)

        async def main(**kwargs):
            t = asyncio.create_task(run_callback_server(test_shutdown_cb))
            await asyncio.sleep(0.1)
            w = []
            for _ in range(2048):
                try:
                    _, writer = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])
                    w.append(writer)
                except:
                    t.cancel()
                    return b'error'

            for writer in w:
                writer.write(b"hello_world")
                await writer.drain()
            t.cancel()
            return b''

        r = asyncio.run(main(ip='127.0.0.1', port=8888))
        self.assertTrue(r == b'')

    def test_conn_establishment_on_closed(self):
        """Test if I recv connection timed out if I try to connect to closed port"""

        async def main(**kwargs):
            await asyncio.sleep(0.1)
            r, w = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])
            s = w.get_extra_info('socket')
            try:
                await asyncio.wait_for(r.read(), timeout=10)
            except asyncio.exceptions.TimeoutError:
                return True
            return False
        r = asyncio.run(main(ip='127.0.0.1', port=8888))
        self.assertTrue(r, "I should have catch a TimeoutError exception")

    def test_shutdown_wr(self):
        """Test if a shutdown write is correctly forwarded on both sides"""
        async def test_shutdown_cb(reader, writer):
            s = writer.get_extra_info('socket')
            data = await reader.read(4096)
            self.assertTrue(data == b'')
            if data == b'':
                s.shutdown(socket.SHUT_WR)
            else:
                writer.write(b'lorem ipsum dolor sit amet' * 10)
                await writer.drain()
            await asyncio.sleep(0.1)

        async def main(**kwargs):
            t = asyncio.create_task(run_callback_server(test_shutdown_cb))

            await asyncio.sleep(0.2)

            # open connection to ip:port
            r, w = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])

            # send something to myself
            s = w.get_extra_info('socket')

            s.shutdown(socket.SHUT_WR)

            await asyncio.sleep(0.01)

            data = await r.read(4096)

            t.cancel()

            return data
        try:
            r = asyncio.run(main(ip='127.0.0.1', port=8888))
        except:
            r = b'err'
        self.assertTrue(r == b'')


    def test_close(self):
        """Test one sided connection close"""
        async def test_close_cb(reader, writer):
            s = writer.get_extra_info('socket')

            data = await reader.read(4096)

            self.assertTrue(data == b'')

            writer.close()

        async def main(**kwargs):
            t = asyncio.create_task(run_callback_server(test_close_cb))

            await asyncio.sleep(0.2)

            # open connection to ip:port
            r, w = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])

            await asyncio.sleep(0.1)
            w.close()
            await asyncio.sleep(0.1)
            t.cancel()


        asyncio.run(main(ip='127.0.0.1', port=8888))

    def test_send_rst(self):
        """Test if a RST is correctly send"""
        async def test_send_rst(reader, writer):
            s = writer.get_extra_info('socket')
            data = await reader.read(4096)
            self.assertTrue(data == b'')
            await asyncio.sleep(0.1)

        async def main(**kwargs):
            t = asyncio.create_task(run_callback_server(test_send_rst))

            await asyncio.sleep(0.2)

            # open connection to ip:port
            r, w = await asyncio.open_connection(host=kwargs['ip'], port=kwargs['port'])

            # send something to myself
            s = w.get_extra_info('socket')
            l_onoff = 1
            l_linger = 0
            s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack('ii', l_onoff, l_linger))
            await asyncio.sleep(0.1)

            w.close()

            await asyncio.sleep(0.1)

            t.cancel()

            return b''
        try:
            r = asyncio.run(main(ip='127.0.0.1', port=8888))
        except:
            r = b'err'
        self.assertTrue(r == b'')


if __name__ == '__main__':

    unittest.main()



